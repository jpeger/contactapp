//
//  DetailViewController.h
//  contactApp
//
//  Created by jpeger on 3/21/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditViewController.h"
#import "MapViewController.h"

@interface DetailViewController : UIViewController
{
    UIImageView* personPhoto;
    UIButton* personPhone;
    UIButton* personAddress;
    UIButton* personEmail;
}

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

