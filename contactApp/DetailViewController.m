//
//  DetailViewController.m
//  contactApp
//
//  Created by jpeger on 3/21/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        //self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
        [personEmail setTitle:[self.detailItem valueForKey:@"email"] forState:UIControlStateNormal];
        [personPhone setTitle:[self.detailItem valueForKey:@"phone"] forState:UIControlStateNormal];
        [personAddress setTitle:[self.detailItem valueForKey:@"address"] forState:UIControlStateNormal];
        [self.navigationItem setTitle:[self.detailItem valueForKey:@"name"]];
        NSData* temp = [self.detailItem valueForKey:@"photo"];
        if (![temp isKindOfClass:[NSNull class]]) {
            [personPhoto setImage:[UIImage imageWithData:temp]];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    personPhoto = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.width-20)];
    UILabel* phone = [[UILabel alloc]initWithFrame:CGRectMake(10, personPhoto.frame.origin.y+personPhoto.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [phone setText:@"Phone #"];
    personPhone = [UIButton buttonWithType:UIButtonTypeSystem];
    [personPhone setFrame:CGRectMake(10, phone.frame.origin.y+phone.frame.size.height+5, self.view.frame.size.width-20, 30)];
    [personPhone addTarget:self action:@selector(callPerson:) forControlEvents:UIControlEventTouchUpInside];
    UILabel* address = [[UILabel alloc]initWithFrame:CGRectMake(10, personPhone.frame.origin.y+personPhone.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [address setText:@"Address"];
    personAddress = [UIButton buttonWithType:UIButtonTypeSystem];
    [personAddress setFrame:CGRectMake(10, address.frame.origin.y+address.frame.size.height+5, self.view.frame.size.width-20, 30)];
    [personAddress addTarget:self action:@selector(getDirections:) forControlEvents:UIControlEventTouchUpInside];
    UILabel* email = [[UILabel alloc]initWithFrame:CGRectMake(10, personAddress.frame.origin.y+personAddress.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [email setText:@"Email"];
    personEmail = [UIButton buttonWithType:UIButtonTypeSystem];
    [personEmail setFrame:CGRectMake(10, email.frame.origin.y+email.frame.size.height+5, self.view.frame.size.width-20, 30)];
    [personEmail addTarget:self action:@selector(emailPerson:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* edit = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editContact:)];
    self.navigationItem.rightBarButtonItem = edit;
    
    [self.view addSubview:personPhoto];
    [self.view addSubview:phone];
    [self.view addSubview:personPhone];
    [self.view addSubview:address];
    [self.view addSubview:personAddress];
    [self.view addSubview:email];
    [self.view addSubview:personEmail];
    
    [self configureView];
}

-(void)callPerson:(id) sender{
    NSString* phoneNumber = [@"telpromt://" stringByAppendingString:[_detailItem valueForKey:@"phone"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)getDirections:(id) sender{
    MapViewController* map = [[MapViewController alloc]init];
    map.detailItem = _detailItem;
    [self.navigationController pushViewController:map animated:YES];
}

-(void)emailPerson:(id) sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_detailItem valueForKey:@"email"]]];
}

-(void)editContact:(id)sender{
    [self performSegueWithIdentifier:@"addEditSegue" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"addEditSegue"]) {
        AddEditViewController *controller = (AddEditViewController *)[segue destinationViewController];
        [controller setDetailItem:_detailItem];
        controller.isEditing = YES;
        controller.managedObjectContext = self.managedObjectContext;
        controller.fetchedResultsController = self.fetchedResultsController;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}
@end
