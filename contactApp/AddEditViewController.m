//
//  AddEditViewController.m
//  contactApp
//
//  Created by jpeger on 3/22/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "AddEditViewController.h"

@interface AddEditViewController ()

@end

@implementation AddEditViewController

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem* save = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveContact:)];
    UIBarButtonItem* takePic = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(takePic:)];
    UIBarButtonItem* loadPic = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(loadImage:)];
    
    NSArray* btnArray = [[NSArray alloc]initWithObjects:save, takePic, loadPic, nil];
    
    self.navigationItem.rightBarButtonItems = btnArray;
    
    contactPhoto = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.width-20)];
    UILabel* phone = [[UILabel alloc]initWithFrame:CGRectMake(10, contactPhoto.frame.origin.y+contactPhoto.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [phone setText:@"Name"];
    contactName = [[UITextField alloc]initWithFrame:CGRectMake(10, phone.frame.origin.y+phone.frame.size.height+5, self.view.frame.size.width-20, 20)];
    [contactName setDelegate:self];
    
    UILabel* address = [[UILabel alloc]initWithFrame:CGRectMake(10, contactName.frame.origin.y+contactName.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [address setText:@"Phone #"];
    contactPhone = [[UITextField alloc]initWithFrame:CGRectMake(10, address.frame.origin.y+address.frame.size.height+5, self.view.frame.size.width-20, 20)];
    [contactPhone setDelegate:self];
    [contactPhone setKeyboardType:UIKeyboardTypePhonePad];
    
    UILabel* email = [[UILabel alloc]initWithFrame:CGRectMake(10, contactPhone.frame.origin.y+contactPhone.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [email setText:@"Address"];
    contactAddress = [[UITextField alloc]initWithFrame:CGRectMake(10, email.frame.origin.y+email.frame.size.height+5, self.view.frame.size.width-20, 20)];
    [contactAddress setDelegate:self];
    [contactAddress setKeyboardType:UIKeyboardTypeDefault];

    UILabel* actuallEmail = [[UILabel alloc]initWithFrame:CGRectMake(10, contactAddress.frame.origin.y+contactAddress.frame.size.height + 5, self.view.frame.size.width - 20, 20)];
    [actuallEmail setText:@"Email"];
    contactEmail = [[UITextField alloc]initWithFrame:CGRectMake(10, actuallEmail.frame.origin.y+actuallEmail.frame.size.height+5, self.view.frame.size.width-20, 20)];
    [contactEmail setDelegate:self];
    [contactEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    // Do any additional setup after loading the view.
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [btn addTarget:self action:@selector(doneEditing) forControlEvents:UIControlEventTouchUpInside];
    
    [contactAddress setInputAccessoryView:btn];
    [contactEmail setInputAccessoryView:btn];
    [contactName setInputAccessoryView:btn];
    [contactPhone setInputAccessoryView:btn];
    
    [self.view addSubview:contactPhoto];
    [self.view addSubview:phone];
    [self.view addSubview:contactName];
    [self.view addSubview:address];
    [self.view addSubview:contactPhone];
    [self.view addSubview:email];
    [self.view addSubview:contactAddress];
    [self.view addSubview:actuallEmail];
    [self.view addSubview:contactEmail];
    
    [self configureView];
}

-(void)doneEditing{
    [self.view endEditing:YES];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        //self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
        [contactEmail setText:[self.detailItem valueForKey:@"email"]];
        [contactName setText:[self.detailItem valueForKey:@"name"]];
        [contactPhone setText:[self.detailItem valueForKey:@"phone"]];
        [contactAddress setText:[self.detailItem valueForKey:@"address"]];
        NSData* temp = [self.detailItem valueForKey:@"photo"];
        if (![temp isKindOfClass:[NSNull class]]) {
            [contactPhoto setImage:[UIImage imageWithData:temp]];
            contactImg = [UIImage imageWithData:temp];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)takePic:(id)sender{
    UIImagePickerController* picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"picker presented for camera");
    }];
}

-(void)loadImage:(id)sender{
    UIImagePickerController* picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"picker presented for photo library");
    }];
}

-(void)saveContact:(id)sender{
    if (_isEditing) {
        NSManagedObjectContext *context = self.managedObjectContext;
        NSManagedObject *contact = _detailItem;
        [contact setValue:contactName.text forKey:@"name"];
        [contact setValue:contactPhone.text forKey:@"phone"];
        [contact setValue:contactAddress.text forKey:@"address"];
        [contact setValue:contactEmail.text forKey:@"email"];
        NSData* temp = [NSData dataWithData:UIImagePNGRepresentation(contactImg)];
        
        [contact setValue:temp forKey:@"photo"];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }else{
        NSManagedObjectContext *context = self.managedObjectContext;
        NSManagedObject *contact = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Event"
                                    inManagedObjectContext:context];
        [contact setValue:contactName.text forKey:@"name"];
        [contact setValue:contactPhone.text forKey:@"phone"];
        [contact setValue:contactAddress.text forKey:@"address"];
        [contact setValue:contactEmail.text forKey:@"email"];
        NSData* temp = [NSData dataWithData:UIImagePNGRepresentation(contactImg)];
        
        [contact setValue:temp forKey:@"photo"];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    contactImg = info[UIImagePickerControllerOriginalImage];
    
    [contactPhoto setImage:contactImg];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
