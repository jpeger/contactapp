//
//  AddEditViewController.h
//  contactApp
//
//  Created by jpeger on 3/22/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AddEditViewController : UIViewController<UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate>
{
    UIImageView* contactPhoto;
    UITextField* contactName;
    UITextField* contactPhone;
    UITextField* contactAddress;
    UITextField* contactEmail;
    UIImage* contactImg;
}

@property (strong, nonatomic) id detailItem;
@property (nonatomic) BOOL isEditing;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
