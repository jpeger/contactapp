//
//  MapViewController.h
//  contactApp
//
//  Created by jpeger on 3/24/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate>
{
    MKMapView* map;
    CLLocationManager* locationManager;
    CLPlacemark* topResult;
    MKPlacemark* placemark;
}

@property (strong, nonatomic) id detailItem;

@end
