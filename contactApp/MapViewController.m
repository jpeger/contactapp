//
//  MapViewController.m
//  contactApp
//
//  Created by jpeger on 3/24/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    map = [[MKMapView alloc ]initWithFrame:self.view.bounds];
    map.delegate = self;
    [map setShowsUserLocation:YES];
    [self.view addSubview:map];
    

    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    [self setLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLocation{
    CLGeocoder* geocoder = [[CLGeocoder alloc]init];
    [geocoder geocodeAddressString:[_detailItem valueForKey:@"address"] completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks && placemarks.count > 0) {
            topResult = [placemarks objectAtIndex:0];
            placemark = [[MKPlacemark alloc]initWithPlacemark:topResult];
            
            MKCoordinateRegion region = map.region;
            region.center = placemark.location.coordinate;
            region.span.longitudeDelta /= 8.0;
            region.span.latitudeDelta /= 8.0;
            [map setRegion:region animated:YES];
            [map addAnnotation:placemark];
        }
    }];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [mapView setRegion:[mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = placemark.location.coordinate;
    point.title = [_detailItem valueForKey:@"name"];
    point.subtitle = [_detailItem valueForKey:@"address"];
    
    [mapView addAnnotation:point];
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f",
                     mapView.userLocation.coordinate.latitude,
                     mapView.userLocation.coordinate.longitude,
                     placemark.location.coordinate.latitude,
                     placemark.location.coordinate.longitude];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"We are here");
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    
    // Button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    button.frame = CGRectMake(0, 0, 23, 23);
    annotationView.rightCalloutAccessoryView = button;
    
    // Image and two labels
    UIView *leftCAV = [[UIView alloc] initWithFrame:CGRectMake(0,0,23,23)];
    annotationView.leftCalloutAccessoryView = leftCAV;
    
    annotationView.canShowCallout = YES;
    
    return annotationView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
